<?php
    $option_array= array('Rock', 'Paper', 'Scissors');
    $user_option = '';
    $computer_option = '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rock, Paper, scissors</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-ms-12">
                <h1>Rock - Paper - Scissors</h1>
                <hr>             
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-ms-6">
                <form action="index.php" method="post">
                    <div class="form-group">
                        <label>Select an option</label>
                        <select class="form-control" name="user_option_control">
                            <?php
                                $_post_user_option_control = $_POST['user_option_control'] ?? '';
                                IF($_post_user_option_control){
                                    $user_option = $_post_user_option_control;
                                    IF($user_option == 'Rock'){
                                        echo 
                                        '<option value="Rock" selected>Rock</option>
                                        <option value="Paper" >Paper</option>
                                        <option value="Scissors" >Scissors</option>';
                                    }
                                    IF($user_option == 'Paper'){
                                        echo 
                                        '<option value="Rock" >Rock</option>
                                        <option value="Paper" selected>Paper</option>
                                        <option value="Scissors" >Scissors</option>';
                                    }
                                    IF($user_option == 'Scissors'){
                                        echo 
                                        '<option value="Rock" >Rock</option>
                                        <option value="Paper" >Paper</option>
                                        <option value="Scissors" selected>Scissors</option>';
                                    }
                                }ELSE{
                                    echo 
                                    '<option value="Rock" selected>Rock</option>
                                    <option value="Paper" >Paper</option>
                                    <option value="Scissors" >Scissors</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Send</button>
                </form>           
            </div>
            <div class="col-lg-6 col-ms-6">
                <p>The computer chose</p>
                <?php
                    $_post_user_option_control = $_POST['user_option_control'] ?? '';
                    IF($_post_user_option_control){
                        // User option
                        $user_option = $_post_user_option_control;
                        // Computer option
                        $computer_option_index = rand(0,2);
                        $computer_option = $option_array[$computer_option_index];
                        echo '<h3>' . $computer_option . '</h3>';
                    }
                ?>
            </div>
         </div>
         <div class="row">
             <div class="col-lg-12 col-md-12">
                 <hr>
             </div>
         </div>
         <div class="row">
             <div class="col-lg-12 col-md-12">
                <?php
                    $_post_user_option_control = $_POST['user_option_control'] ?? '';
                    IF($_post_user_option_control){    
                        IF($user_option == $computer_option){
                            echo 
                            '<div class="alert alert-primary" role="alert">
                                You tied! ' . $user_option .' is equal to '. $computer_option .'
                            </div>';
                        }ELSE{
                            IF(($user_option == 'Rock' && $computer_option == 'Scissors')
                                || ($user_option == 'Scissors' && $computer_option == 'Paper')
                                || ($user_option == 'Paper' && $computer_option == 'Rock') ){
                                    echo 
                                    '<div class="alert alert-success" role="alert">
                                        You won! ' . $user_option .' beats '. $computer_option .' 
                                    </div>';
                            }ELSE{
                                echo 
                                '<div class="alert alert-danger" role="alert">
                                    You lose! ' . $user_option .' lose against '. $computer_option .'
                                </div>';
                            }
                        }
                    }
                ?>
             </div>
         </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>